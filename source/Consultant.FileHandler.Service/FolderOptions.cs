namespace Consultant.FileHandler.Service
{
    public class FolderOptions
    {
        public string Output { get; set; }

        public string Input { get; set; }
    }
}