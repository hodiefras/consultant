using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Consultant.FileHandler.Service.Data;
using Consultant.FileHandler.Service.Handlers;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Consultant.FileHandler.Service
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                var jobQueue = new ConcurrentQueue<FileJob>();
                var taskQueue = new ConcurrentQueue<FileBatchTask>();

                var folderWatcher = new FolderWatcher(jobQueue);
                var fileHandler = new Handlers.FileHandler(resultFolderPath);
                var letterFileCalculator = new LetterFileCalculator(fileHandler, jobQueue, taskQueue);
                folderWatcher.Watch(monitoringFolderPath);

                var t1 = new Thread(() => letterFileCalculator.Calculate(stoppingToken));
                var t2 = new Thread(() => letterFileCalculator.Calculate(stoppingToken));
                var t3 = new Thread(() => letterFileCalculator.Calculate(stoppingToken));
                var t4 = new Thread(() => letterFileCalculator.Calculate(stoppingToken));

                t1.Start();
                t2.Start();
                t3.Start();
                t4.Start();
            }
        }
    }
}
