﻿namespace Consultant.FileHandler.Service.Data
{
    public class FileBatchTask
    {
        public char[] Batch { get; set; }
        public FileJob FileJob { get; set; }
    }
}