﻿using System.Collections.Concurrent;
using System.IO;
using Consultant.FileHandler.Data;

namespace Consultant.FileHandler.Services
{
    public class FolderWatcher
    {
        private readonly ConcurrentQueue<FileJob> _jobQueue;
        public FolderWatcher(ConcurrentQueue<FileJob> jobQueue)
        {
            _jobQueue = jobQueue;
        }
        public void Watch(string monitoringFolderPath)
        {
            var watcher = new FileSystemWatcher
            {
                Path = monitoringFolderPath, 
                NotifyFilter = NotifyFilters.LastWrite, 
                Filter = "*.*"
            };
            watcher.Created += WatcherOnCreated;
            watcher.EnableRaisingEvents = true;
        }

        private void WatcherOnCreated(object sender, FileSystemEventArgs e)
        {
            _jobQueue.Enqueue(new FileJob() { FileNamePath = e.FullPath });
        }
    }
}