﻿using System.Collections.Concurrent;
using System.IO;
using Consultant.FileHandler.Data;

namespace Consultant.FileHandler.Services
{
    public class FileHandler
    {
        private readonly string _outputFolderPath;
        private readonly int batchSize = 2000;

        public FileHandler(string outputFolderPath)
        {
            _outputFolderPath = outputFolderPath;
        }

        public void WriteCountToFile(string fileName, long letterCount)
        {
            var name = Path.GetFileName(fileName);
            var path = Path.Combine(_outputFolderPath, name);
            using var streamWriter = new StreamWriter(path);
            streamWriter.WriteLine(letterCount);
        }

        public void ReadBatchesForJob(FileJob fileJob, ConcurrentQueue<FileBatchTask> fileBatchQueue)
        {
            using var streamReader = new StreamReader(fileJob.FileNamePath);
            char[] buffer = new char[batchSize];

            bool isNextCharAvailable = streamReader.Peek() != -1;
            int totalCharCount = 0;
            while (isNextCharAvailable)
            {
                var readCharCount = streamReader.Read(buffer, 0, batchSize);
                totalCharCount += readCharCount;
                if (readCharCount <= 0 || streamReader.Peek() == -1)
                {
                    isNextCharAvailable = false;
                    fileJob.CharCount = totalCharCount;
                }
                fileBatchQueue.Enqueue(new FileBatchTask() { Batch = buffer, FileJob = fileJob });
            }
        }
    }
}