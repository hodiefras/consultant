﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using Consultant.FileHandler.Data;

namespace Consultant.FileHandler.Services
{
    public class LetterFileCalculator
    {
        private readonly ConcurrentQueue<FileJob> _jobQueue;
        private readonly ConcurrentQueue<FileBatchTask> _taskQueue;
        private readonly FileHandler _fileHandler;

        public LetterFileCalculator(FileHandler fileHandler, ConcurrentQueue<FileJob> jobQueue, ConcurrentQueue<FileBatchTask> taskQueue)
        {
            _fileHandler = fileHandler;
            _jobQueue = jobQueue;
            _taskQueue = taskQueue;
        }

        public void Calculate(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                if (CalculateToFile())
                {
                    continue;
                }
                Coordinate();
            }
        }

        private bool CalculateToFile()
        {
            if (!_taskQueue.TryDequeue(out var task))
            {
                return false;
            };

            var letterCount = task.Batch.LongCount(char.IsLetter);

            Interlocked.Add(ref task.FileJob.LetterCount, letterCount);
            Interlocked.Add(ref task.FileJob.HandledCharCount, task.Batch.Length);
            if (task.FileJob.CharCount != null && task.FileJob.CharCount == task.FileJob.HandledCharCount)
            {
                _fileHandler.WriteCountToFile(task.FileJob.FileNamePath, task.FileJob.LetterCount);
            }

            return true;
        }

        private void Coordinate()
        {
            if (!_jobQueue.TryDequeue(out var job))
            {
                return;
            };

            _fileHandler.ReadBatchesForJob(job, _taskQueue);
        }
    }
}