﻿using System.Collections.Concurrent;
using System.Threading;
using Consultant.FileHandler.Data;
using Consultant.FileHandler.Services;

namespace Consultant.FileHandler
{
    public class Worker
    {
        public void Work(string monitoringFolderPath, string resultFolderPath, CancellationToken token)
        {
            var jobQueue = new ConcurrentQueue<FileJob>();
            var taskQueue = new ConcurrentQueue<FileBatchTask>();

            var folderWatcher = new FolderWatcher(jobQueue);
            var fileHandler = new Services.FileHandler(resultFolderPath);
            var letterFileCalculator = new LetterFileCalculator(fileHandler, jobQueue, taskQueue);
            folderWatcher.Watch(monitoringFolderPath);

            var t1 = new Thread(() => letterFileCalculator.Calculate(token));
            var t2 = new Thread(() => letterFileCalculator.Calculate(token));
            var t3 = new Thread(() => letterFileCalculator.Calculate(token));
            var t4 = new Thread(() => letterFileCalculator.Calculate(token));

            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();
        }
    }
}