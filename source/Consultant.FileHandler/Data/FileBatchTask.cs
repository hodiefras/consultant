﻿namespace Consultant.FileHandler.Data
{
    public class FileBatchTask
    {
        public char[] Batch { get; set; }
        public FileJob FileJob { get; set; }
    }
}