﻿namespace Consultant.FileHandler.Data
{
    public class FileJob
    {
        public string FileNamePath { get; set; }

        public long LetterCount;

        public long HandledCharCount;

        public long? CharCount;
    }
}